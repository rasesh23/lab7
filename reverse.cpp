/*#include <iostream>

void reverseArray(char arr[], char first, char last)
{
  while (first < last)
    {
      int temp = arr[first];
      arr[first] = arr[last];
      arr[last] = temp;
      first++;
      last--;
    }
}

void printArray(char arr[], int size)
{
  for (int i=0; i<size; i++)
    {
      std::cout << arr[i] << " ";
      std::cout <<endl;
    }
}

int main ()
{
  char arr[] = {H,A,R,R,Y};
  int n = sizeof(arr) / sizeof(arr[0]);

  //Print original array
  printArray(arr ,n);

  reverseArray(arr, 0, n-1);
  std::cout << "Reversed array is" <<endl;

  //Print reversed array
  printArray(arr, n);

  return 0;
}

*/

#include <iostream>
#include <cstring>

void reverse(char s[])
{
  char aux;
  int i;
  for (i=0; i<strlen(s)/2; i++)
    {
      aux = s[i];
      s[i] = s[strlen(s)-i-1];
      s[strlen(s)-i-1] = aux;
    }
}

int main()
{
  char s[] = "Harry";
  reverse(s);
  std::cout << s << "\n";
}
    

