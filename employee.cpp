#include <iostream>

struct Employee
{
  std::string first_name;
  std::string last_name;
  std::string id;
  std::string email;
  std::string phone_num;
  double salary;
  Employee* manager;
};

int main()
{
  Employee emp1, emp2, emp3;

  emp1.first_name = "Donald";
  emp1.last_name = "Duck";
  emp1.id = "DD1234567";
  emp1.email = "DD420@live.mdx.ac.uk";
  emp1.phone_num = "+23051234567";
  emp1.salary = 50000.00;
  emp1.manager = NULL;
    
  emp2.first_name = "Johnny";
  emp2.last_name = "Sins";
  emp2.id = "JS1234567";
  emp2.email = "Js69@live.mdx.ac.uk";
  emp2.phone_num = "+23059876543";
  emp2.salary = 40000.00;
  emp2.manager = &emp1;

  //emp3.first_name = "Scooby";
  //emp3.last_name = "Doo";
  //emp3.id = "SD1234567";
  //emp3.email = "SD100@live.mdx.ac.uk";
  //emp3.phone_num = "+23051357924";
  //emp3.salary = 45000.00;
  //emp3.manager = &emp1;
  
  emp3 = {"Scooby","Doo","SD1234567","SD100@live.mdx.ac.uk","+23051357924",45000.00, &emp1};

  std::cout<< emp2.first_name << "'s Manager is: "<< emp2.manager->first_name;

  return 0;
}
