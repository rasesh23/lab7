#include <iostream>

struct Employee
{
  std::string first_name;
  std::string last_name;
  std::string id;
  std::string email;
  std::string phone_num;
  double salary;
  Employee* manager;
};

void setEmployee(Employee* employee,std::string first_name,std::string last_name,std::string id,std::string email,std::string phone_num,double salary)
{
  employee->first_name = first_name;
  employee->last_name = last_name;
  employee->id = id;
  employee->email = email;
  employee->phone_num = phone_num;
  employee->salary = salary;
  employee->manager = NULL;
}

void staffinput(Employee* employee)
{
  std::string first_name;
  std::string last_name;
  std::string id;
  std::string email;
  std::string phone_num;
  double salary;

  std::cout << "\nFirst Name: ";
  std::cin >> first_name;

  std::cout << "\nSurname: ";
  std::cin >> last_name;

  std::cout << "\nId: ";
  std::cin >> id;

  std::cout << "\nEmail: ";
  std::cin >> email;

  std::cout << "\nPhone Number: ";
  std::cin >> phone_num;

  std::cout << "\nSalary: ";
  std::cin >> salary;

  setEmployee(employee,first_name,last_name,id,email,phone_num,salary);
}

void displayStaff(Employee* employees, int num_staff)
{
  int i = 0;
  std::cout << "ID \t Name\n";

  for (i=0; i<num_staff; i++)
    {
      std::cout << employees[i].id << "\t" << employees[i].first_name <<std::endl;
    }
}

int main()
{
  int num_staff = 0;
  int i = 0;

  std::cout << "Please enter number of staff: ";
  std::cin >> num_staff;

  Employee* employees = new Employee[num_staff];

  for (i=0; i<num_staff; i++)
    {
      staffinput(&employees[i]);
    }

  displayStaff(employees,num_staff);
  delete [] employees;
}





