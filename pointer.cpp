#include <iostream>

int main()
{
  double var = 2.4;
  double* ptrVar = &var;

  std::cout <<"Variable value: " <<var<< "address: "<<&var<<std::endl;
  std::cout <<"Pointer value: " <<ptrVar<< "adress: "<<&ptrVar<<" dereference: " <<* ptrVar;
}
